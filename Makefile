# A very quick and dirty way of managing a certificate authority, (Requires openssl)
#
# The authority.* files represent the certificates for the Certificate authority and if lost/deleted will render all certs signed under it invalid.
# The domain names for the authority and the client should not be the same.
#
# Usage:
#   Create the authority certificate files using this command, this should only be run once.
#     make authority.crt
#
#   Create a client certificate (signed by the authority) with this command;
#     make certname.crt
#   anything ending in ".crt", except for "authority.crt" will be signed by the authority certificate.
#
# commands taken from the following resource;
#   - https://learn.microsoft.com/en-us/azure/application-gateway/self-signed-certificates


AUTHROITY=authority

# create the root certificate
${AUTHROITY}.crt: ${AUTHROITY}.csr ${AUTHROITY}.key 
	openssl x509 -req -sha256 -days 365 -in ${AUTHROITY}.csr -signkey ${AUTHROITY}.key -out ${AUTHROITY}.crt

# create an encrypted key (which is given to a CA when requesting a certificate)
%.key:
	openssl ecparam -out $@ -name prime256v1 -genkey
.PRECIOUS: %.key

# create a signing request (a public key given to the CA when requesting a certificate)
%.csr: %.key
	openssl req -new -sha256 -key $< -out $@

# create the certificate and sign it with the root key
%.crt: %.csr ${AUTHROITY}.crt ${AUTHROITY}.key
	openssl x509 -req -in $< -CA ${AUTHROITY}.crt -CAkey ${AUTHROITY}.key -CAcreateserial -out $@ -days 365 -sha256

clean:
	rm *.key *.csr *.crt
