# Poor man's certificate authority

A very quick and dirty way of managing a certificate authority, (Requires openssl)

The authority.\* targets represent the certificates for the Certificate authority and if lost/deleted will render all certs signed under it invalid.
The domain names for the authority and the client should not be the same.

## Usage
Create the authority certificate files using this command, this should only be run once.
```
make authority.crt
```

Create a client certificate (signed by the authority) with this command;
```
make certname.crt
```
anything ending in ".crt", except for "authority.crt" will be signed by the authority certificate.

commands taken from the following resource;
  - https://learn.microsoft.com/en-us/azure/application-gateway/self-signed-certificates

